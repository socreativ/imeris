<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme-by-socreativ
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php the_field('google_font', 'option'); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<style>
		:root{--font: <?= get_field('fontname', 'options') ?>}
	</style>

	<?php wp_body_open(); ?>
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'theme-by-socreativ'); ?></a>

	<header id="main-header" class="container-fluid bkg-dark-grey px-5md-">
		<div class="row d-flex justify-content-between">
			<div class="main-logo col-xl-4 col-lg-4 col-md-4 col-5 h-100 d-flex">
				<?php if (get_field('logo_isSVG', 'options')) { ?>
					<a href="<?= get_home_url() ?>" class="h-100"> <?php echo get_field('logo_svg', 'options'); ?> </a>
				<?php } else { ?>
					<a class="h-100" href="<?= get_home_url() ?>">
						<?php if (wp_is_mobile()) : ?>
							<img class="filter-grey" src="<?php echo get_field('logo', 'options')['dark']['sizes']['small']; ?>" height="<?php echo get_field('logo_dark', 'options')['height'] ?>" width="<?php echo get_field('logo_dark', 'options')['width'] ?>" alt="<?php echo get_field('logo_dark', 'options')['alt'] ?>">
						<?php else : ?>
							<img class="filter-grey" src="<?php echo get_field('logo', 'options')['dark']['url']; ?>" height="<?php echo get_field('logo_dark', 'options')['height'] ?>" width="<?php echo get_field('logo_dark', 'options')['width'] ?>" alt="<?php echo get_field('logo_dark', 'options')['alt'] ?>">
						<?php endif; ?>
					</a>
				<?php } ?>
			</div>
			<div class="col-xl-6 col-lg-8 col-7 real-one d-flex">
				<?php if (!my_wp_is_mobile()) : ?>
					<nav class="global-menu w-100 h-100">
						<?php
						wp_nav_menu(array(
							'theme_location' => 'desktop-main-menu',
							'container_class' => 'main-menu-container',
							'container' => 'ul',
							'menu_id' => 'main-menu-container',
							'menu_class' => 'main-menu justify-content-end list-unstyled main-menu-container d-flex h-100',
						));
						?>
					</nav>
				<?php else : ?>
					<div class="burger-menu anim-300">
						<div class="menu-toggle d-block d-lg-none position-relative">
							<!-- <p class="m-0">MENU</p> -->
							<span class="hamb-line anim-300"></span>
							<span class="hamb-line anim-300"></span>
							<span class="hamb-line anim-300 m-0"></span>
							<span class="crose-line anim-300"></span>
							<span class="crose-line crose-line2 anim-300"></span>
						</div>
					</div>
					<nav class="mobile-main-menu anim-800">
						<div class="mobile-menu-container">
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'mobile-main-menu',
										'container_class' => 'mobile-container-menu anim-800',
										'container' => 'ul',
										'menu_id' => 'mobile-menu',
										'menu_class' => 'mobile-menu anim-300 d-flex list-unstyled text-white w-100',
									)
								);
							?>
						</div>
						
						
					</nav>
				<?php endif; ?>
			</div>
		</div>
	</header>