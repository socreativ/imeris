<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  

    <div class="jumbotron">
        <div class="greyscale"></div>
	    <?php theme_by_socreativ_post_thumbnail(); ?>
    </div>



	<div class="blog-content">
        <a class="back-to-archive" href="<?= get_post_type_archive_link('post'); ?>">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>ic_keyboard_arrow_left_24px</title>
                <g fill="#ffffff" class="nc-icon-wrapper">
                    <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"></path>
                </g>
            </svg>
            Retour aux actualités
        </a>

        <header>
            <p><span class="cat_name"><?= print_categories(get_the_category()); ?></span> | <?= get_the_date(); ?></p>
            <h1><?= get_the_title(); ?></h1>
        </header>
		<?php the_content(); ?>
	</div>

</article>