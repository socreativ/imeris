<?php
/**
 * Template part for displaying page content in archive.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */


?>

<div id="post-<?php the_ID(); ?>" <?php post_class('post-card'); ?>>

    <a href="<?= get_post_permalink(); ?>"><div class="post__wrapper">
        <?= get_the_post_thumbnail(); ?>
        <div class="post__information">
            <div class="info"><span class="cat_name"><?= print_categories(get_the_category()); ?></span> | <?= get_the_date(); ?></div>
            <p class="card__title"><?= get_the_title(); ?></p>
        </div>
    </div></a>

</div>
