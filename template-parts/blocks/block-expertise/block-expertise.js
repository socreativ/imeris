("use strict");
(function($, root, undefined) {
    $(function() {
        $(document).ready(function() {
            $('.block-expertise__number img').each(function() {
                if (window.innerWidth > 516)
                    gsap.to(this, { y: -400, scrollTrigger: { scrub: true } });
            });
        });
    });
})(jQuery, this);