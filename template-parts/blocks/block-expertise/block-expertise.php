<?php

/**
 * expertise Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'expertise-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'expertise';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$title = get_field('title') ?: 'Your title here...';
$texte = get_field('texte') ?: 'Your text here...';
$image = get_field('image') ?: 'your image';
$number = get_field('number') ?: 'number';
$button = get_field('button');

?>
<section class="container">
    <div class="row d-flex justify-content-md-center">
           <div class="col-12 col-md-8 col-lg-5">
               <div class="block-expertise__image h-100">
                   <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="h-100 position-absolute">
               </div>
           <div class="block-expertise__number">
                    <img src="<?= $number['url'] ?>" alt="<?= $number['alt'] ?>" >
                </div>
           </div>
            <div class="block-expertise__content col-12 col-md-8 col-lg-5">
               <div class="block-expertise__info bkg-dark-grey w-100 px-5 py-5">
                    <h3 class="block-expertise__title text-white pb-3">
                    <?= $title ?>
                    </h3>
                    <p class="block-expertise__text text-white pb-5">
                        <?= $texte ?>
                    </p>
                    <a href="<?= $button['url'] ?>" class="en-savoir-plus-btn button-light">
                        <?= $button['title'] ?>
                    </a>
               </div>
        </div>
    </div>
</section>