<?php

/**
 * expertise Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'expertise-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'expertise';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$svg = get_field('svg') ?: '';
$zone = get_field('zone') ?: '';
$nvl_aqt = get_field('nvl_aqt') ?: '';
$text = get_field('text') ?: '';
$link = get_field('link') ?: '';
$bg_img = get_field('bg_img');

?>
<section class="block-position d-flex position-container ">
        <div class="container">
            <div class="row justify-content-center bkg-dark-grey pt- 0 pb-5 py-sm-5 py-md-5 py-lg-5">
                <div class="col-12 col-md-10 col-lg-6 my-5 map-container">
                    <div class="force-map position-relative">
                        <div class="bkg-grey2 text-center position-relative mb-5">
                            <?= $svg ?>
                            <p class="position-absolute text-gold map-text"><?= $nvl_aqt ?></p>
                        </div>
                    </div>
                    <p class="text-white mb-5 mx-3 mx-lg-0"><?= $text ?></p>  
                    <a href="<?= $link['url'] ?>" class="en-savoir-plus-btn button-light"><?= $link['title'] ?></a>  
                </div>
            </div>
         </div>
   <div class="col-12 col-sm-6 col-md-6 col-lg-6 force-bg" style="background-image:url('<?= $bg_img['url']; ?>')">
                
    </div>
</section>