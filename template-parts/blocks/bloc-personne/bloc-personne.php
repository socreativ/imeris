<?php

/**
 * expertise Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'expertise-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'expertise';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$lettre = get_field('lettre') ?: '';
$personne = get_field('personne') ?: '';
$titre = get_field('titre') ?: '';
$v_text = get_field('v_text') ?: '';
$h_text = get_field('h_text') ?: '';
$imgalignment = get_field('gauche') ?'img-gauche': 'img-right';
?>
<section class="container-fluid d-flex cabinet pb-5 <?= $imgalignment ?>">
    <div class="container">
        <div class="row align-content-center flex-column">
        <div class="cabinet-images position-relative col-12 col-lg-6">
            <img src="<?= $lettre['url'] ?>" alt="<?= $lettre['alt'] ?>" class="position-absolute cabinet-lettre">
            <img src="<?= $personne['url'] ?>" alt="<?= $personne['alt'] ?>">
        </div>
        <div class="col-12 col-lg-6 cabinet-text text-center">
            <div class="lnr-bg">
                <h3 class="person-title"><?= $titre ?></h3>
                <p class=""><?= $v_text ?></p>
            </div>
            <div class="text-container position-relative">
                <div class="hidden-text">
            <p class=""><?= $h_text ?></p>
            </div>
            <p class="cabinet-button button-dark en-savoir-plus-btn">Lire plus</p>
            </div>
        </div>
        </div>
    </div>
</section>