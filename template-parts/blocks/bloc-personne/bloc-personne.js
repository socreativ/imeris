("use strict");
(function($, root, undefined) {
    $(function() {
        $(document).ready(function() {
            $('.cabinet').each(function() {
                $('.cabinet-lettre').each(function() {
                    if (window.innerWidth > 516)
                        gsap.to(this, { y: -400, scrollTrigger: { scrub: true } });
                });
            });

            $(".en-savoir-plus-btn").click(function() {
                var that = $(this);
                var readmorecontainer = $(this).parent();
                var hiddentext = readmorecontainer.find('.hidden-text')
                hiddentext.slideToggle(300, function() {
                    if (hiddentext.is(':visible')) {
                        that.text('Fermer');
                    } else {
                        that.text('Lire plus');
                    }
                });
            });
        });
    });
})(jQuery, this);

// function docReady(fn) {
//     if (
//         document.readyState === "complete" ||
//         document.readyState === "interactive"
//     ) {
//         setTimeout(fn, 1);
//     } else {
//         document.addEventListener("DOMContentLoaded", fn);
//     }
// }

// function displayText() {
//     const toggle = Array.from(document.querySelectorAll(".cabinet-button"));
//     toggle.forEach((t, i) => {
//         let hiddenText = document.querySelectorAll(".hidden-text")[i];
//         let cabinetButton = document.querySelectorAll(".cabinet-button")[i];
//         t.addEventListener("click", () => {
//             if (document.querySelectorAll(".hidden-text")[i].classList.contains('hidden')) {
//                 setTimeout(() => {
//                     cabinetButton.classList.remove("opacity-on");
//                     hiddenText.classList.remove("opacity-on");
//                     cabinetButton.classList.add("opacity-off");
//                     hiddenText.classList.add("opacity-off");
//                 }, 0);
//                 setTimeout(() => {
//                     cabinetButton.classList.remove("opacity-off");
//                     hiddenText.classList.remove("opacity-off");
//                     cabinetButton.classList.add("opacity-on");
//                     hiddenText.classList.add("opacity-on");
//                 }, 200);
//                 hiddenText.classList.remove("hidden");
//                 cabinetButton.classList.remove("move-up");
//                 cabinetButton.innerText = 'Fermer';
//             } else {
//                 setTimeout(() => {
//                     cabinetButton.classList.remove("opacity-on");
//                     hiddenText.classList.remove("opacity-on");
//                     cabinetButton.classList.add("opacity-off");
//                     hiddenText.classList.add("opacity-off");
//                 }, 0);
//                 setTimeout(() => {
//                     cabinetButton.classList.remove("opacity-off");
//                     cabinetButton.classList.add("opacity-on");
//                 }, 200);
//                 hiddenText.classList.add("hidden");
//                 cabinetButton.classList.add("move-up");
//                 cabinetButton.innerText = 'Lire plus';

//             }
//         });
//     });
// }

// docReady(() => {
//     displayText();
// });