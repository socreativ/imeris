<?php

$bkg_type = get_field('bkg_type');
$css_class = $bkg_type ? 'text-white' : 'text-black';
$bkg = $bkg_type ? get_field('bkg_img') : get_field('bkg_color') ;
$bkg_alhpa = get_field('bkg_alpha');


?>

<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="steps <?= $css_class ?> <?php if(isset($block['className'])) echo $block['className']; ?>" <?php if(!$bkg_type) echo 'style="background-color:'.$bkg.'"' ?>>

    <?php if($bkg_type): ?>
        <div class="steps__background">
            <div class="greyscale" style="opacity: .<?= $bkg_alhpa ?>"></div>
            <img src="<?= $bkg['url']; ?>" alt="<?= $bkg['alt']; ?>">
        </div>
    <?php endif; ?>

    <div class="container">
        <h2 class="steps__title"><?= get_field('title'); ?></h2>

        <?php if(have_rows('steps')): ?>
            <div class="steps__toggle">
                <?php while(have_rows('steps')): the_row(); ?>
                    <?php if(get_row_index() != 1): ?> <span class="separator"></span> <?php endif; ?>
                    <p class="<?php if(get_row_index() == 1) echo 'select' ?>"><?= get_row_index(); ?></p>
                <?php endwhile; ?>
            </div>


            <div class="steps__content">
                <?php while(have_rows('steps')): the_row(); ?>
                    <div class="steps__paragraph <?php if(get_row_index() == 1) echo 'visible'; ?>">
                        <h3><?= get_sub_field('s_title'); ?></h3>
                        <p><?= get_sub_field('s_content'); ?></p>
                    </div>
                <?php endwhile; ?>
            </div>

            <?php if(have_rows('btns')): ?>
                    <div style="height: 10vh;"></div>
                <?php while(have_rows('btns')) : the_row(); ?>
                    <a class="wp-block-button__link has-white-color bkg-gold no-border-radius" href="<?= get_sub_field('btn')['url']; ?>"><?= get_sub_field('btn')['title']; ?></a>
                <?php endwhile; ?>
            <?php endif; ?>

        <?php endif; ?> 

    </div>
    <?php if(!my_wp_is_mobile()): ?>
        <span class="big_index">1</span>
    <?php endif; ?>



</section>