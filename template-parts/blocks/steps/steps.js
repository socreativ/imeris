"use-strict";

function docReady(fn) {
  if (
    document.readyState === "complete" ||
    document.readyState === "interactive"
  ) {
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

function ListenForToggle() {
  const toggle = Array.from(document.querySelectorAll(".steps__toggle p"));
  const index = document.querySelector(".big_index");
  toggle.forEach((t, i) => {
    t.addEventListener("click", () => {
      toggle.map((a) => {
        if (t !== a && a.matches(".select")) a.classList.remove("select");
      });
      t.classList.add("select");
      document
        .querySelector(".steps__paragraph.visible")
        .classList.remove("visible");
      document
        .querySelectorAll(".steps__paragraph")
        [i].classList.add("visible");
      index.style.opacity = 0;
      setTimeout(() => {
        index.innerText = i + 1;
        index.style.opacity = 1;
      }, 350);
    });
  });
}

docReady(() => {
  ListenForToggle();
});
