<?php

/**
 * expertise Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'expertise-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'expertise';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$main_title = get_field('main-title') ?: '';
$button = get_field('button');


?>
<div class="container">
<p class="block-slider__title text-lg-left text-md-left text-center"><?= $main_title ?></p>
</div>
<section class="container-fluid block-slider d-flex hide-scrollbar">
        <div class="container position-relative">
            <div class="d-block justify-content-center py-5 h-100">
            <div class="modele-slider h-100">
            <?php

// Check rows exists.
if( have_rows('block-repeater') ): 

    // Loop through rows.
     while( have_rows('block-repeater') ) : the_row();

        // Load sub field value.
        $title = get_sub_field('titre');
        $text = get_sub_field('text');
        // Do something...
    ?>
        <div class="col-11 col-md-7 col-lg-4 bkg-dark-grey text-white px-4 h-100 mr-4">
            <div class="position-relative gold-after pt-5 pb-4">
                <h4 class="block-slider__bloc-title"><?= $title ?></h4>
            </div>
            <div class="pb-3 pt-4">
                 <p class="block-slider__bloc-text"><?= $text ?></p>
            </div>
        </div>

  
    <?php endwhile;

// No value.
else :
    // Do something...
endif; ?>
            </div>
            </div>
            <div class="modele-slider-arrows">   
            <div class="slider-prev-arrow modele-prev-arrows">
                    <svg class="appear" width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <title>Arrow</title>
                        <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#000" fill-rule="nonzero">
                                <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                                    <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>

                <div class="slider-next-arrow modele-next-arrows">
                    <svg class="appear" width="100%" height="100%" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <title>Arrow</title>
                        <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Group-19" transform="translate(0.000000, -0.000000)" fill="#000" fill-rule="nonzero">
                                <g id="arrow-down" transform="translate(6.000000, 3.000000)">
                                    <polygon id="Stroke-1" transform="translate(5.500000, 8.389086) rotate(-90.000000) translate(-5.500000, -8.389086) " points="5.5 10.5378859 12.2704628 3.3160589 13.7295372 4.6839411 5.5 13.4621141 -2.7295372 4.6839411 -1.2704628 3.3160589"></polygon>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
         </div>
</section>
<div class="container d-flex justify-content-end">
    <a href="<?= $button['url']; ?>" class="wp-block-button__link has-white-color bkg-gold no-border-radius"><?= $button['title']; ?></a>
</div>






