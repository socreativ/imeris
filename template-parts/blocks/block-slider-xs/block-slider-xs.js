("use strict");
(function($, root, undefined) {
    $(function() {
        $(document).ready(function() {
            if (window.innerWidth > 992) {
                $(".modele-slider").slick({
                    infinite: false,
                    arrows: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    autoplay: false,
                    prevArrow: $(".modele-prev-arrows"),
                    nextArrow: $(".modele-next-arrows"),
                });
            } else {
                gsap.to(".modele-slider", {
                    scrollTrigger: {
                        trigger: ".block-slider__link",
                        start: "top 100%",
                        once: true,
                        // toggleActions: "once none none none",
                    },
                    x: -20,
                    duration: 0.35,
                    repeat: 1,
                    yoyo: true,
                })
            }
        });
    });
})(jQuery, this);