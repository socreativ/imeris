<section id="partenaires" data-scroll-section>

    <h2 class="partenaires-title"><?php the_field('title'); ?></h2>
    <div class="container position-relative">
    <?php if(have_rows('partenaires')): ?>
    <div class="partenaires-slider text-center">
        <?php while(have_rows('partenaires')): the_row(); ?>
            <?php if(get_sub_field('logo')['isSVG']): ?>
                <?php echo get_sub_field('logo')['svg'] ?>
            <?php else: ?>
                <img class="skip-lazy" data-skip-lazy="" src="<?php echo get_sub_field('logo')['image']['url'] ?>" alt="<?php echo get_sub_field('logo')['image']['alt'] ?>" width="250" height="125"/>
            <?php endif; ?>
        <?php endwhile; ?>
    </div>
    <div class="partenaires-slider-arrows">   
        <div class="slider-prev-arrow partenaires-prev-arrows">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="17.492" viewBox="0 0 10 17.492">
                <path id="Icon_ionic-ios-arrow-back" data-name="Icon ionic-ios-arrow-back" d="M18.236,14.937,11.618,8.324a1.25,1.25,0,0,1,1.77-1.765l7.5,7.493a1.248,1.248,0,0,1,.036,1.724l-7.53,7.545a1.25,1.25,0,0,1-1.77-1.765Z" transform="translate(-11.251 -6.194)" fill="#d0b082"/>
            </svg>
        </div>

        <div class="slider-next-arrow partenaires-next-arrows">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="17.492" viewBox="0 0 10 17.492">
                <path id="Icon_ionic-ios-arrow-back" data-name="Icon ionic-ios-arrow-back" d="M18.236,14.937,11.618,8.324a1.25,1.25,0,0,1,1.77-1.765l7.5,7.493a1.248,1.248,0,0,1,.036,1.724l-7.53,7.545a1.25,1.25,0,0,1-1.77-1.765Z" transform="translate(-11.251 -6.194)" fill="#d0b082"/>
            </svg>
        </div>
    </div>
    <?php endif; ?>
    </div>
</section>
