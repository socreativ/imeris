("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {
      $(".partenaires-slider").each(function () {
        $(this).slick({
          dots: false,
          infinite: true,
          autoplay: true,
          speed: 300,
          slidesToShow: 3,
          slideToScroll: 1,
          draggable: false,
          arrows: true,
          prevArrow: $(".partenaires-prev-arrows"),
          nextArrow: $(".partenaires-next-arrows"),
          responsive: [
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
        });
      });
    });
  });
})(jQuery, this);
