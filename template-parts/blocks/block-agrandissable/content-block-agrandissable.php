<?php

/**
* Ticket Block Template.
*
* @param   array $block The block settings and attributes.
* @param   string $content The block inner HTML (empty).
* @param   bool $is_preview True during AJAX preview.
* @param   (int|string) $post_id The post ID this block is saved to.
*/

// Create id attribute allowing for custom "anchor" value.
$id = 'faq-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'faq';
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>

<?php if ( have_rows('toggle_section') ): ?>
  <div class="schema-faq wp-block-yoast-faq-block custom-faq">
    <?php while ( have_rows('toggle_section') ) : the_row(); ?>
      <div id="<?php the_sub_field("id-accordion");?>" class="schema-faq-section <?php if( get_sub_field('fermer') ) { ?>default-close<?php }?>">
        
          <h3 class="d-flex m-0 fs-16 schema-faq-question has-h-3-sans-serif-font-size">
          <?php the_sub_field( "titre_block_agrandissable" ); ?>
          </h3>

        <div class="schema-faq-answer">
          <?php the_sub_field( "contenu_block_agrandissable" ); ?>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
<?php endif; ?>


