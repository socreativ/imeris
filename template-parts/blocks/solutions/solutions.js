("use strict");
(function($, root, undefined) {
    $(function() {
        $(document).ready(function() {
            let isMobile = window.innerWidth < 768 ? true : false;
            if (isMobile) {
                setTimeout(function() {
                    $('.anchor-container').animate({ left: '-=25px' }, 300);
                    $('.anchor-container').animate({ left: '+=25px' }, 300);
                }, 2000);
                gsap.to(".anchor-container", {
                    scrollTrigger: {
                        trigger: ".footer-container",
                        start: "top 150%",
                        toggleActions: "restart reverse reverse pause",
                        scrub: true,
                    },
                    y: 1000,
                    duration: 0.1,
                })
            }
            $('.solutions-button').on('click', function() {
                $('.solutions-button').removeClass('solutions-selected');
                $(this).addClass('solutions-selected');
            });
        });
    });
})(jQuery, this);

function docReady(fn) {
    if (
        document.readyState === "complete" ||
        document.readyState === "interactive"
    ) {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function setGoldBg() {
    let goldBg = Array.from(document.querySelectorAll('.solutions-button'));
    goldBg[0].classList.add('solutions-selected');
}
docReady(() => {
    setGoldBg();
});