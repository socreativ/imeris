<?php

/**
 * solutions Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'solutions-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'solutions';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$titre = get_field('titre') ? '' : '';
$button = get_field('button');


?>

<section id="solutions" class="container-fluid">
    <div class="container">
        <?php if(my_wp_is_mobile()): ?>
            <div class="d-flex flex-column mr-4 anchor-container hide-scrollbar">
                <?php if(have_rows('solutions')): ?>
                    <?php while(have_rows('solutions')): the_row(); ?>
                        <?php $image = get_sub_field('image'); ?>
                        <?php $anchor = get_sub_field('s_text'); ?>
                        <a href="#<?= $image['id'] ?>"" class="solutions-button text-gold d-flex align-items-center justify-content-between mr-3 mx-lg-0 col-5 col-lg-12 col-md-12">
                            <div class="d-flex align-items-center">
                                <p class="mb-0 mr-3 row-index">0
                                    <?= get_row_index(); ?>
                                </p>
                                <?= $anchor ?>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="17.492"
                                viewBox="0 0 10 17.492">
                                <path id="Icon_ionic-ios-arrow-back" data-name="Icon ionic-ios-arrow-back"
                                    d="M18.236,14.937,11.618,8.324a1.25,1.25,0,0,1,1.77-1.765l7.5,7.493a1.248,1.248,0,0,1,.036,1.724l-7.53,7.545a1.25,1.25,0,0,1-1.77-1.765Z"
                                    transform="translate(-11.251 -6.194)" fill="#d0b082" />
                            </svg>
                        </a>
                    <?php endwhile; ?>
                <?php endif; ?>

                <a href="<?= $button['url'] ?>" class="solutions-button text-gold text-center">
                    <?= $button['title'] ?>
                </a>

            </div>
        <?php endif; ?>
        <div class="row">
            <div class="archive__content col-lg-5 col-md-5">
                <aside class="left-col">
                <?php if (!my_wp_is_mobile()) : ?>
                    <div class="sticky">
                        <h1>
                            <?= get_field('titre');  ?>
                        </h1>
                        <div class="d-flex flex-column mr-4 anchor-container hide-scrollbar">
                            <?php if(have_rows('solutions')): ?>
                            <?php while(have_rows('solutions')): the_row(); ?>
                            <?php $image = get_sub_field('image'); ?>
                            <?php $anchor = get_sub_field('s_text'); ?>
                            <a href="#<?= $image['id'] ?>"" class=" solutions-button text-gold d-flex align-items-center
                                justify-content-between mr-3 mx-lg-0 col-5 col-lg-12 col-md-12">
                                <div class="d-flex align-items-center">
                                    <p class="mb-0 mr-3 row-index">0
                                        <?= get_row_index(); ?>
                                    </p>
                                    <?= $anchor ?>
                                </div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="17.492"
                                    viewBox="0 0 10 17.492">
                                    <path id="Icon_ionic-ios-arrow-back" data-name="Icon ionic-ios-arrow-back"
                                        d="M18.236,14.937,11.618,8.324a1.25,1.25,0,0,1,1.77-1.765l7.5,7.493a1.248,1.248,0,0,1,.036,1.724l-7.53,7.545a1.25,1.25,0,0,1-1.77-1.765Z"
                                        transform="translate(-11.251 -6.194)" fill="#d0b082" />
                                </svg>
                            </a>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <a href="<?= $button['url'] ?>" class="solutions-button text-gold text-center">
                                <?= $button['title'] ?>
                            </a>
                        </div>
                    </div>
                    <?php else : ?>
                        <div class="sticky">
                        <h1>
                            <?= get_field('titre');  ?>
                        </h1>
                    </div>
                        <?php  if(!my_wp_is_mobile()): ?>
                        <div class="d-flex flex-column mr-4 anchor-container hide-scrollbar">
                            <?php if(have_rows('solutions')): ?>
                                <?php while(have_rows('solutions')): the_row(); ?>
                                    <?php $image = get_sub_field('image'); ?>
                                    <?php $anchor = get_sub_field('s_text'); ?>
                                    <a href="#<?= $image['id'] ?>"" class="solutions-button text-gold d-flex align-items-center justify-content-between mr-3 mx-lg-0 col-5 col-lg-12 col-md-12">
                                        <div class="d-flex align-items-center">
                                            <p class="mb-0 mr-3 row-index">0
                                                <?= get_row_index(); ?>
                                            </p>
                                            <?= $anchor ?>
                                        </div>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="17.492"
                                            viewBox="0 0 10 17.492">
                                            <path id="Icon_ionic-ios-arrow-back" data-name="Icon ionic-ios-arrow-back"
                                                d="M18.236,14.937,11.618,8.324a1.25,1.25,0,0,1,1.77-1.765l7.5,7.493a1.248,1.248,0,0,1,.036,1.724l-7.53,7.545a1.25,1.25,0,0,1-1.77-1.765Z"
                                                transform="translate(-11.251 -6.194)" fill="#d0b082" />
                                        </svg>
                                    </a>
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <a href="<?= $button['url'] ?>" class="solutions-button text-gold text-center">
                                <?= $button['title'] ?>
                            </a>

                        </div>
                        <?php endif; ?> 
                    <?php endif; ?>
            </div>
            </aside>

            <div class="col-12 col-lg-7 col-md-7 text-container">
                <?php if(have_rows('solutions')): ?>
                <?php while(have_rows('solutions')): the_row(); ?>
                <?php $image = get_sub_field('image'); ?>
                <?php $s_titre = get_sub_field('solutions_titre'); ?>
                <?php $s_text = get_sub_field('solutions_texte'); ?>
                <div class="solutions-content">
                    <img id="<?= $image['id'] ?>" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"
                        class="solutions-image">
                    <div class="d-flex mt-5">

                        <h3 class="solutions-subtitle m-0"><span class="text-gold mr-2">0
                                <?= get_row_index(); ?>
                            </span>
                            <?= $s_titre ?>
                        </h3>
                    </div>
                    <p class="mt-3 lh-3">
                        <?= $s_text ?>
                    </p>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

