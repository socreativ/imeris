("use strict");
(function($, root, undefined) {
    $(function() {
        $(document).ready(function() {
            let isMobile = window.innerWidth < 768 ? true : false;
            if (isMobile) {
                gsap.to(".services-row", {
                    scrollTrigger: {
                        trigger: ".block-expertise__content",
                        start: "top 100%",
                        once: true,
                    },
                    x: -20,
                    duration: 0.35,
                    repeat: 1,
                    yoyo: true,
                })
            }
            $('.solutions-button').on('click', function() {
                $('.solutions-button').removeClass('solutions-selected');
                $(this).addClass('solutions-selected');
            });
            $('.block-service__lien').mouseover(function() {
                $(this).addClass('white-fill');
            });
            $('.block-service__lien').mouseout(function() {
                $(this).removeClass('white-fill');
            });
        });
    });
})(jQuery, this);