<?php

/**
 * Testimonial Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
// $id = 'testimonial-' . $block['id'];
// if( !empty($block['anchor']) ) {
//     $id = $block['anchor'];
// }

// Create class attribute allowing for custom "className" and "align" values.
// $className = 'testimonial';
// if( !empty($block['className']) ) {
//     $className .= ' ' . $block['className'];
// }
// if( !empty($block['align']) ) {
//     $className .= ' align' . $block['align'];
// }

// Load values and assign defaults.
$text = get_field('service-texte') ?: 'Your text here...';
$image = get_field('image') ?: 295;
$role = get_field('services-lien') ?: 'your link...';


?>
<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="appear-section <?php if(isset($block['className'])) echo $block['className']; ?>">
    <div class="container services-container hide-scrollbar">
        <div class="row services-row hide-scrollbar">
            <?php 
                // Check rows exists.
                if( have_rows('service-repeater') ):

                 while( have_rows('service-repeater') ) : the_row();
                $lien = get_sub_field('lien');
                $image = get_sub_field('image', 'option');
                $texte = get_sub_field('texte');
                $test =  file_get_contents( $image );
        // Do something...
                  ?>
                  <div class="block-services col-sm-3 col-md-3 col-lg-3">
                    <a href="<?= $lien['url']; ?>" class="block-service__lien h-100 d-flex justify-content-around flex-column align-items-center py-4">
                    <object class="block-service__img"><?=  file_get_contents( $image ); ?></object>
                    <p class="block-service__text"><?= $texte ?></p>
                    <p class="block-service__plus text-white">+</p>
                </a>
                  </div>
              <?php  endwhile; 
               
// No value.
                else :
    // Do something...
                endif;
            ?>
        </div>
    </div>
</section>