("use strict");
(function($, root, undefined) {
    $(function() {
        $(document).ready(function() {

            $(".schema-faq-question").click(function() {
                $(".schema-faq-question").removeClass("question-open");
                $(this).parent().find(".schema-faq-answer").slideToggle(300);
                $(this).toggleClass("question-open");
            });

            $('.archive__content').each(function() {
                $('.post-card:nth-of-type(even)').each(function() {
                    if (window.innerWidth > 516)
                        gsap.to(this, { y: -50, scrollTrigger: { scrub: true } });
                });
            });

            $('.category__toggle').click(function() {
                $(this).toggleClass('w');
                $('.category__sidebar').toggleClass('open');
            })

        });
    });
})(jQuery, this);