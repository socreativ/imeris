<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package socreativ-theme
 */

?>
	<!-- BEGIN FOOTER -->
	<footer id="colophon" class="site-footer">
	
	<div class="footer-container">

		<div class="logo-footer container p-0 position-relative">
			<div class="row m-0">
				<div class="col-lg-4 col-md-6 col-12 site-title mb-5 mb-lg-0 text-center text-md-left">
					<?php if(get_field('name', 'options')): ?>
						<h2><?php echo get_field('name', 'options'); ?></h2>
					<?php endif; ?>
				</div>
				<div class="col-lg-4 col-md-6 col-12 site-desc mb-5 mb-lg-0 text-center text-md-left">
					<?php if(get_field('desc', 'options')): ?>
						<p><?php echo get_field('desc', 'options'); ?></p>
					<?php endif; ?>
				</div>
				<div class="col-lg-4 col-md-6 col-12 insta-footer justify-content-end mb-5 mb-lg-0 text-center text-md-left">
				<?php if(get_field('desc', 'options')): ?>
					<?php wp_nav_menu(
						array(
							'theme_location' => 'menu-RS',
							'container_class' => '',
							'container' => 'ul',
							'menu_id' => 'rs-menu',
							'menu_class' => 'lh-15 ml-0 pl-0 list-unstyled',
						)
					);?>
					<?php endif; ?>
				</div>
			</div>
		</div>	

		<div class="container">
			<div class="row py-4">
				<div class="col-12 col-md-6 col-lg-4 d-flex flex-column mb-5 mb-lg-2 mb-md-0">
					<?php
					if(!get_field('is_custom_menu_footer')){
						wp_nav_menu(
							array(
								'theme_location' => 'footer',
								'container_class' => '',
								'container' => 'ul',
								'menu_id' => 'footer-menu',
								'menu_class' => 'lh-15 ml-0 pl-0 list-unstyled',
							)
						);
					}
					else{ ?>
						<ul id="footer-menu" class="lh-15 fs-18 ml-0 pl-0 list-unstyled">

					<?php if(have_rows('custom_menu_footer')){
							while (have_rows('custom_menu_footer')){
								the_row();
								$link = get_sub_field('custom_menu_footer_link'); ?>

								<li class="menu-item"><a href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?> </a></li>
					<?php	}
						} ?>
						</ul>
					<?php }
					?>
				</div>
				<div class="col-12 col-md-6 col-lg-4 d-flex flex-column mb-5 mb-lg-2 mb-md-0 text-center text-lg-left">
						<?php if ( have_rows('adresse_contact','option') ): ?>
						    <?php while ( have_rows('adresse_contact','option') ) : the_row(); ?>
									<?php if (get_sub_field('titre')): ?>
							      <h4 class="fw-700 mb-3 address-title"><?php the_sub_field('titre'); ?></h4>
									<?php endif; ?>
									<?php
									$link = get_sub_field('adresse');
									if( $link ):
									    $link_url = $link['url'];
									    $link_title = $link['title'];
									    $link_target = $link['target'] ? $link['target'] : '_self';
										?>
									    <a class="d-flex flex-column text-white address-link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" rel="nofollow">
												<?php the_sub_field('adresse_detaillee'); ?>
										</a>
									    <a class="anim-300 adresse-link d-none flex-column align-items-start text-white mt-3 fs-12" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" rel="nofollow">
												<span class="anim-300">
													<?php echo esc_html( $link_title ); ?>
												</span>
											</a>
									<?php endif; ?>
						    <?php endwhile; ?>
						<?php endif; ?>
						<?php dynamic_sidebar( 'footer-widget-2' ); ?>
						
						
				</div>
				<div class="col-12 col-md-6 col-lg-4 d-flex flex-column mb-2 mb-md-0 text-center text-lg-right">
					<!-- TEL -->
					<?php if ( have_rows('contact_tel','option') ): ?>
							<?php while ( have_rows('contact_tel','option') ) : the_row(); ?>
								<?php if (get_sub_field('titre')): ?>
									<h4 class="fs-26 fw-700 mb-3 footer-number"><?php the_sub_field('titre'); ?></h4>
								<?php endif; ?>
								<?php
								$link = get_sub_field('tel');
								if( $link ):
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										<a class="d-block text-white fs-26 fw-700" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" rel="nofollow">
											<?php echo esc_html( $link_title ); ?>
										</a>
								<?php endif; ?>
							<?php endwhile; ?>
					<?php endif; ?>
					<hr id="hr-contact">
					<!-- MAIL -->
					<?php if ( have_rows('contact_mail','option') ): ?>
							<?php while ( have_rows('contact_mail','option') ) : the_row(); ?>
								<?php if (get_sub_field('titre')): ?>
									<h4 class="fs-16 fw-700 mb-3"><?php the_sub_field('titre'); ?></h4>
								<?php endif; ?>
								<?php
								$link = get_sub_field('adresse');
								if( $link ):
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										<a class="d-block text-white mt-1 fs-16 fw-700" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" rel="nofollow">
											<?php echo esc_html( $link_title ); ?>
										</a>
								<?php endif; ?>
							<?php endwhile; ?>
					<?php endif; ?>
					<?php if (get_field('horaires','option')): ?>
						<div class="">
							<?php the_field( 'horaires','option' ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="sub-footer">
		<div class="container">
			<?php if(get_field('last_p', 'options') != '' && get_field('custom_text_footer') != ''){ ?>
			<div class="fs-14 py-5">
				<?php
				if(!get_field('is_custom_text_footer')){
				 echo get_field('last_p', 'options');
				}
				else{
					echo get_field('custom_text_footer');
				} ?>
			</div>
			<?php } ?>
		</div>
		<div class="sub-footer-legacy text-center fs-12 py-2">
			<?php dynamic_sidebar( 'sub-footer-widget' ); ?>
		</div>
	</div>

	</footer>
	<!-- END FOOTER -->

	<?php if (get_field('page_wip') && !is_user_logged_in()) : ?>
	<!-- BUILDING MODE -->
	<div class="building-mode">
		<img class="building-img" src="">
		<div class="building-greyscale"></div>
		<div class="text-building-mode no-bkg">
			<div class="mt-5vh text-center op-0">
				<h2 class="text-white fs-48 fw-700"><?php the_title(); ?></h2>
				<p class="text-white fs-22 mb-5vh">Page en construction</p>
				<p class="text-white fs-21 mb-5vh"><?php echo get_field('text_page_building'); ?><p>
				<a class="custom-link anim-300" href="<?php echo get_site_url(); ?>">
					Retour à l'accueil
				</a>
			</div>
		</div>
	</div>
		<style media="screen">
			body.building-mode{
				height: 100vh;
				overflow: hidden;
			}
			.building-mode{
				background-color: #000;
				width: 100vw;
				height: 100vh;
				position: fixed;
				z-index: 99999;
				top: 0;
				left: 0;
			}
			.building-img{
				width: 100%;
				height: 100%;
			}
			.building-greyscale{
				background-color: #000000AA;
				width: 100vw;
				height: 100vh;
				position: fixed;
				top: 0;
				left: 0;
				z-index: 8;
			}
			.text-building-mode {
		position: fixed;
		z-index: 9;
		top: 50%;
		transform: translate(0%, -50%);
		text-align: center;
		color: #fff;
		width: 100%;
		height: 25%;
			}
		</style>

	<!-- END BUILDING MODE -->
	<?php endif; ?>

<?php wp_footer(); ?>


</body>
</html>