<?php
/**
 * theme-by-socreativ functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package theme-by-socreativ
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'theme_by_socreativ_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function theme_by_socreativ_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on theme-by-socreativ, use a find and replace
		 * to change 'theme-by-socreativ' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'theme-by-socreativ', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );


		

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'desktop-main-menu' => esc_html__( 'Menu princial Ordinateur', 'theme-by-socreativ' ),
				'mobile-main-menu' => esc_html__( 'Menu princial Mobile', 'theme-by-socreativ' ),
				'menu-RS' => esc_html__( 'Social', 'theme-by-socreativ' )
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'theme_by_socreativ_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'theme_by_socreativ_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function theme_by_socreativ_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'theme_by_socreativ_content_width', 640 );
}
add_action( 'after_setup_theme', 'theme_by_socreativ_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_by_socreativ_widgets_init() {
	register_sidebar( array(
        'name'          => __( 'Sub Footer', 'theme-by-socreativ' ),
        'id'            => 'sub-footer-widget',
        'description'   => __( 'Add widgets for your blog here.', 'theme-by-socreativ' ),
        'before_widget' => '<section id="%1$s" class="%2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title h5">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'theme_by_socreativ_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function theme_by_socreativ_scripts() {
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.custom.min.css', true, '1.0', 'all');
	
	wp_enqueue_style( 'theme-by-socreativ-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'theme-by-socreativ-style', 'rtl', 'replace' );

	wp_enqueue_style( 'helpers', get_stylesheet_directory_uri() . '/assets/css/helpers.css', true, '1.0', 'all');
	wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/assets/css/main.css', true, '1.0', 'all');
	wp_enqueue_style( 'archive', get_stylesheet_directory_uri() . '/assets/css/archive.css', true, '1.0', 'all');
	wp_enqueue_style( 'blog', get_stylesheet_directory_uri() . '/assets/css/blog.css', true, '1.0', 'all');
	wp_enqueue_style( 'gutenberg', get_stylesheet_directory_uri() . '/assets/css/gutenberg.css', true, '1.0', 'all');
	wp_enqueue_style( 'header', get_stylesheet_directory_uri() . '/assets/css/header.css', true, '1.0', 'all');
	wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/assets/css/footer.css', true, '1.0', 'all');
	wp_enqueue_style( 'responsive', get_stylesheet_directory_uri() . '/assets/css/responsive.css', true, '1.0', 'all');
	wp_enqueue_style( 'contact', get_stylesheet_directory_uri() . '/assets/css/contact.css', true, '1.0', 'all');
	wp_enqueue_style( 'slickSliderStyle', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css', true, '1.0', 'all');
	wp_enqueue_style( 'slickSliderTheme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css', true, '1.0', 'all');

	wp_enqueue_script( 'gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js', ['jquery'], '1.0.0', true);

	wp_enqueue_script( 'slickSlider', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', ['jquery'], '1.0.0', false);

	wp_enqueue_script( 'ScrollTrigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/ScrollTrigger.min.js', ['jquery'], '1.0.0', true);
	wp_enqueue_script( 'globalScript', get_template_directory_uri() . '/assets/js/script.js', ['jquery'], '1.0.0', false);

	wp_enqueue_script( 'theme-by-socreativ-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), _S_VERSION, true );

	wp_enqueue_script( 'theme-by-socreativ-header', get_template_directory_uri() . '/js/header.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'theme_by_socreativ_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/* Allow SVG files */
function wpc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'wpc_mime_types');

/* -------------------------------------------
				GUTENBERG STYLE
--------------------------------------------- */

// Gutenberg Editor width
function gb_gutenberg_admin_styles() {
    echo '
        <style>
            /* Main column width */
            .wp-block {
                max-width: 1220px;
            }

            /* Width of "wide" blocks */
            .wp-block[data-align="wide"] {
                max-width: 1980px;
            }

            /* Width of "full-wide" blocks */
            .wp-block[data-align="full"] {
                max-width: none;
            }
        </style>
    ';
}
add_action('admin_head', 'gb_gutenberg_admin_styles');


/*********************************************************************
                        PLUGIN DEPENDENCIES
 ********************************************************************/

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'socreativ_register_required_plugins' );


function socreativ_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'Advanced Custom Fields Pro', // The plugin name.
			'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/plugins/advanced-custom-fields-pro.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo'
		),

		array(
			'name'        => 'Contact Form 7',
			'slug'        => 'contact-form-7'
		),

		array(
			'name'        => 'Flamingo',
			'slug'        => 'flamingo'
		),

		array(
			'name'        => 'iThemesecurity',
			'slug'        => 'better-wp-security',
			'force_activation'   => true
		),

		array(
			'name'        => 'ACF Content Analyse for Yoast SEO',
			'slug'        => 'acf-content-analysis-for-yoast-seo'
		),

		array(
			'name'        => 'Yoast Duplicate Post',
			'slug'        => 'duplicate-post',
		)

	);


	$config = array(
		'id'           => 'theme-by-socreativ',    		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      		// Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', 		// Menu slug.
		'parent_slug'  => 'themes.php',            		// Parent menu slug.
		'capability'   => 'edit_theme_options',    		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    		// Show admin notices or not.
		'dismissable'  => false,                   		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   		// Automatically activate plugins after installation or not.
	);

	tgmpa( $plugins, $config );

}


add_filter( 'plugin_action_links', 'blacklist_plugins', 10, 4 );
function blacklist_plugins( $actions, $plugin_file, $plugin_data, $context ) {
 
    if ( array_key_exists( 'activate', $actions ) && in_array( $plugin_file, array(
		'elementor/elementor.php',
		'divi-builder/divi-builder.php',
		'wp-bakery/wp-bakery.php',
		'visualcomposer/plugin-wordpress.php'
	)))
        unset( $actions['activate'] );
 
    return $actions;
}

function deactive_blacklist_plugin(){

	if(!function_exists('is_plugin_active')){
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	}


	$blacklist_plugins = array(
		'elementor/elementor.php',
		'divi-builder/divi-builder.php',
		'wp-bakery/wp-bakery.php',
		'visualcomposer/plugin-wordpress.php'
	);

	foreach ($blacklist_plugins as $index => $plugin) {
		if(is_plugin_active($plugin)){
			deactivate_plugins($plugin);
		}
	}

}
deactive_blacklist_plugin();

/*********************************************************************
                            ACF Params
 ********************************************************************/


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' => __('Options'),
		'menu_title' => __('Options'),
		'menu_slug' => 'options',
		'position' => '31'
	));
}

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

	/* Add gategorie for blocks */
	function my_block_category($categories, $post){
		return array_merge(
			$categories,
			array(
				array(
					'slug' => 'custom-blocks',
					'title' => __('Custom Block', 'custom-blocks'),
				),
			)
		);
	}
	add_filter('block_categories_all', 'my_block_category', 10, 2);

	//custom block ACF
	require_once get_template_directory() . '/inc/custom-blocks.php';

}



/*********************************************************************
						 CUSTOM FUNCTIONS
 ********************************************************************/

function my_wp_is_mobile() {
    static $is_mobile;

    if ( isset($is_mobile) )
        return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
        $is_mobile = false;
    } elseif (
        strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    } else {
        $is_mobile = false;
    }

    return $is_mobile;
}


function acf_img($img, $size="large", $class = null, $id = null, $attr = null){

	if(!empty($img)){

		if(isset($img['sizes'][$size])){
			$url = $img['sizes'][$size];
		}

		$srcset = wp_get_attachment_image_srcset($img['id'], $size);
		$sizes = wp_get_attachment_image_sizes($img['id'], $size);
		$alt = $img['alt'];
		$attributes = "";
		foreach ($attr as $k => $v) {
			$attributes .= "$k=\"$v\" ";
		}

		if(function_exists('wp_get_attachment_image_srcset')){
			$img = "<img id=\"$id\" class=\"$class\" src=\"$url\" srcset=\"$srcset\" alt=\"$alt\" sizes=\"$sizes\" $attributes />";
		}

		return $img;

	}

}

// Get ACF image field and create new attribute to object
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects( $items, $args ) {
	foreach( $items as &$item ) {
		$icon = get_field('menu_icon', $item);
		if( $icon ) {
			$item->icon = $icon;
		}
		$item->title .= $item->icon;
	}
	return $items;
}

function print_categories($a){
    $str = "";
    foreach ($a as $k => $e) {
        $str .= $k == 0 ? $e->name : ', '.$e->name;
    }
    return $str;
}


// CUSTOM GUTENBERG CLASS
register_block_style(
'core/button',
	array(
	'name'  => 'imeris-button-light',
	'label' => __( 'Imeris Button', 'wp-imeris-button-light' ),
	)
);
register_block_style(
	'core/paragraph',
		array(
		'name'  => 'clamp',
		'label' => __( 'clamp', 'wp-clamp' ),
		)
	);
register_block_style(
'core/button',
	array(
	'name'  => 'imeris-button-dark',
	'label' => __( 'Imeris Button', 'wp-imeris-button-dark' ),
	)
);
register_block_style(
	'core/group',
		array(
		'name'  => 'container',
		'label' => __( 'Content in grid', 'wp-container' ),
		)
	);
register_block_style(
	'core/image',
		array(
		'name'  => 'stroke-image',
		'label' => __( 'Image avec cadre doré', 'wp-stroke-image' ),
		)
	);

