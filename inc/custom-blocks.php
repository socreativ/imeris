<?php 

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

         // Espacement
         acf_register_block_type(array(
            'name'              => 'spacing',
            'title'             => __('Espacement'),
            'description'       => __('margin block'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-break',
            'keywords'          => array( 'margin', 'space', 'vh'),
            'render_template'   => '/template-parts/blocks/spacing/spacing.php',
            'supports'          => array( 'anchor' => true),
        ));
        acf_register_block_type(array(
            'name'              => 'vertical-bar',
            'title'             => __('Vertical Bar'),
            'description'       => __('Vertical Bar'),
            'render_template'   => 'template-parts/blocks/vertical-bar/vertical-bar.php',
            'category'          => 'custom-blocks',
            'icon'              => 'tide',
            'keywords'          => array( 'Vertical bar', 'margin', 'vertical'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.css',
        ));
 
        acf_register_block_type(array(
            'name'              => 'slider',
            'title'             => __('Slider'),
            'description'       => __('Slider'),
            'render_template'   => 'template-parts/blocks/slider/slider.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'slider', 'video', 'picture'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.css',
        ));

        acf_register_block_type(array(
            'name'              => 'block-agrandissable',
            'title'             => __('block-agrandissable'),
            'description'       => __('block agrandissable'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/block-agrandissable/content-block-agrandissable.php',
            'supports'          => array( 'anchor' => true),
        ));

        acf_register_block_type(array(
            'name'              => 'block-services',
            'title'             => __('block-services'),
            'description'       => __('block-services'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/block-services/block-services.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/block-services/block-services.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/block-services/block-services.css',
        ));

        acf_register_block_type(array(
            'name'              => 'block-expertise',
            'title'             => __('block-expertise'),
            'description'       => __('block-expertise'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/block-expertise/block-expertise.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/block-expertise/block-expertise.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/block-expertise/block-expertise.css',
        ));

        acf_register_block_type(array(
            'name'              => 'block-position',
            'title'             => __('block-position'),
            'description'       => __('block-position'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/block-position/block-position.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/block-position/block-position.css',
        ));

        acf_register_block_type(array(
            'name'              => 'block-steps',
            'title'             => __('Étapes'),
            'description'       => __('Étapes'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-ol',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/steps/steps.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/steps/steps.js',
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/steps/steps.css',
        ));

        acf_register_block_type(array(
            'name'              => 'block-slider-xs',
            'title'             => __('block-slider-xs'),
            'description'       => __('block-slider-xs'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-ol',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/block-slider-xs/block-slider-xs.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/block-slider-xs/block-slider-xs.js',
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/block-slider-xs/block-slider-xs.css',
        ));

        acf_register_block_type(array(
            'name'              => 'partenaires',
            'title'             => __('partenaires'),
            'description'       => __('partenaires'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/partenaires/partenaires.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/partenaires/partenaires.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/partenaires/partenaires.css',
        ));

        acf_register_block_type(array(
            'name'              => 'solutions',
            'title'             => __('solutions'),
            'description'       => __('solutions'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/solutions/solutions.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/solutions/solutions.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/solutions/solutions.css',
        ));
        acf_register_block_type(array(
            'name'              => 'bloc-personne',
            'title'             => __('bloc-personne'),
            'description'       => __('bloc-personne'),
            'category'          => 'custom-blocs',
            'icon'              => 'editor-ol',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/bloc-personne/bloc-personne.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/bloc-personne/bloc-personne.js',
            'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/bloc-personne/bloc-personne.css',
        ));


        add_action('acf/init', 'register_acf_block_types');

    }


?>