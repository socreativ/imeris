<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package theme-by-socreativ
 */

function get_last_post(){
	return get_posts(array('numberposts' => 1, 'post_status' => 'publish', 'order' => 'DESC'))[0];
}

function get_first_post(){
	return get_posts(array('numberposts' => 1, 'post_status' => 'publish', 'order' => 'ASC'))[0];
}


get_header();
?>

	<main id="primary" class="site-main">

		<div class="container pt-4 position-relative">

			<?php while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', get_post_type() );


			endwhile; ?>
		
		
		</div>

	


		<div class="posts-navigation">

			<div class="pn__container">
				<div class="previous-post">
					<?php $pp = get_previous_post() ? get_previous_post() : get_last_post(); ?>
					<a href="<?= get_permalink($pp->ID) ?>" class="pp__link">
						<p>
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>ic_keyboard_arrow_left_24px</title>
								<g fill="#ffffff" class="nc-icon-wrapper">
									<path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"></path>
								</g>
							</svg>
							Article précédent
						</p>
						<p><?= $pp->post_title ?></p>
						<div class="info"><span class="cat_name"><?= print_categories(get_the_category($pp->ID)); ?></span> | <?= get_the_date('', $pp->ID); ?></div>
					</a>
				</div>
	
				<div class="next-post">
					<?php $np = get_next_post() ? get_next_post() : get_first_post(); ?>
					<a href="<?= get_permalink($np->ID) ?>" class="np__link">
						<p>
							Article suivant
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>ic_keyboard_arrow_left_24px</title>
								<g fill="#ffffff" class="nc-icon-wrapper">
									<path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"></path>
								</g>
							</svg>
						</p>
						<p><?= $np->post_title ?></p>
						<div class="info"><span class="cat_name"><?= print_categories(get_the_category($np->ID)); ?></span> | <?= get_the_date('', $np->ID); ?></div>
					</a>
				</div>
			</div>
		</div>

	</main><!-- #main -->

<?php
get_footer();
