("use strict");
(function($, root, undefined) {
    $(function() {
        $(document).ready(function() {

            var isMobile = window.innerWidth < 768 ? true : false;          

            var lastScrollTop = 0;
            $(window).scroll(function() {
                var st = $(this).scrollTop();
                if (st > 100) {
                    if (st > lastScrollTop) {
                        $('body').addClass("down-scroll");
                        $('body').removeClass("up-scroll");
                        if (st > 100 && !$('body').hasClass('sticky')) {
                            $('body').addClass('sticky');
                        }
                    } else {
                        $('body').addClass("up-scroll");
                        $('body').removeClass("down-scroll");
                        if (st < $(window).height() - 50 && $('body').hasClass('sticky') && !$('body').hasClass('f-sticky')) {
                            $('body').removeClass('sticky');
                        }
                    }
                    lastScrollTop = st;
                }
            });


            /**
             *   Blog
             */

            $('.single-link-container').each(function() {
                let bkg = $(this).find('.archive-img-cover').attr('src');
                let target = $('.attachment-post-thumbnail');

                $(this).on('mouseover', function() {

                    if (target.attr('src') != bkg) {
                        TweenMax.to(".attachment-post-thumbnail", 0.3, {
                            autoAlpha: 0,
                            scale: 1.05
                        });

                        target.attr('src', bkg);

                        setTimeout(() => {
                            TweenMax.to(".attachment-post-thumbnail", 0.3, {
                                autoAlpha: 1,
                                scale: 1
                            });
                        }, 300);
                    }
                });
            });

            if (!isMobile) {
                $('.single-link-container[parallax="1"]').each(function() {
                    var parallaxController = new ScrollMagic.Controller({});
                    new ScrollMagic.Scene({
                            triggerElement: ".container-index",
                            triggerHook: 0.65,
                            duration: "100%",
                        })
                        .setTween($(this), { y: -150, ease: Linear.easeNone })
                        .addTo(parallaxController);
                })

                $('.single-parallax').each(function() {
                    var parallaxController = new ScrollMagic.Controller({});
                    new ScrollMagic.Scene({
                            triggerElement: ".single-container",
                            triggerHook: 0.65,
                            duration: "100%",
                        })
                        .setTween($(this), { y: -150, ease: Linear.easeNone })
                        .addTo(parallaxController)
                });
            }

            /**
             *  CPT Archive & Single
             */


            $('.filtre-toggle').each(function() {
                $(this).click(function() {
                    $('.filtre').toggleClass('open');
                })
            })

            $('.archive-single-product').each(function() {
                let bkg = $(this).find('.wp-post-image').attr('src');
                let target = $('.attachment-post-thumbnail.archive-bkg');

                $(this).on('mouseover', function() {

                    if (target.attr('src') != bkg) {
                        TweenMax.to(".attachment-post-thumbnail.archive-bkg", 0.3, {
                            autoAlpha: 0,
                            scale: 1.05
                        });

                        target.attr('src', bkg);

                        setTimeout(() => {
                            TweenMax.to(".attachment-post-thumbnail.archive-bkg", 0.3, {
                                autoAlpha: 1,
                                scale: 1
                            });
                        }, 300);
                    }
                })
            })

            $('.desc-tab-tgl').click(function() {
                $('.desc-tab').removeClass('d-none').addClass('d-block');
                $('.additional-tab').removeClass('d-block').addClass('d-none');
                $(this).addClass('active');
                $('.additional-tab-tgl').removeClass('active');
            });
            $('.additional-tab-tgl').click(function() {
                $('.additional-tab').removeClass('d-none').addClass('d-block');
                $('.desc-tab').removeClass('d-block').addClass('d-none');
                $(this).addClass('active');
                $('.desc-tab-tgl').removeClass('active');
            })

            $('.product-gallery-sticky').each(function() {
                $(this).slick({
                    autoplay: false,
                    dots: true,
                    fade: false,
                    infinite: false
                })
            })

            /**
             *   FAQ GUTENBERG
             */
            //   $('.schema-faq-question').click(function(){
            //     $(this).parent().find('.schema-faq-answer').slideToggle( 300 );
            //     $(this).toggleClass( 'question-open' );
            //   });

            /**
             *  notification / modal
             */
            $('.notification').each(function() {
                const that = this;
                const delay = $(this).attr('data-delay');
                if (sessionStorage.getItem('cookieSeen') != 'shown') {
                    setTimeout(() => {
                        $(this).removeClass('closed');
                    }, delay * 1000);
                }
                $('.notification .close').each(function() {
                    $(this).click(() => {
                        $(that).addClass('closed');
                        sessionStorage.setItem('cookieSeen', 'shown')
                    });
                });
            })
            $('.custom-modal').each(function() {
                    const that = this;
                    const delay = $(this).attr('data-delay');
                    if (sessionStorage.getItem('cookieSeen') != 'shown') {
                        setTimeout(() => {
                            $(this).removeClass('closed');
                            $('body').addClass('modalOpen');
                        }, delay * 1000);
                    }
                    $('.custom-modal .close').each(function() {
                        $(this).click(() => {
                            $(that).addClass('closed');
                            $('body').removeClass('modalOpen');
                            sessionStorage.setItem('cookieSeen', 'shown')
                        });
                    });
                })
                /**
                 * Burger menu toggle
                 */
            $('.menu-toggle').each(function() {
                $(this).click(function() {
                    $('body').toggleClass('nav-open');
                });
            });

        });
    });
})(jQuery, this);