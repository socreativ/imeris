<?php
/**
 * The template for displaying archive posts pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

get_header();
?>

<main id="primary" class="site-main">

   <div class="archive__content">
       <?php if(!my_wp_is_mobile()): ?>
        <aside class="left-col">
            <div class="sticky">
                <h1><?= _e('Les Actualités'); ?></h1>

                <div class="category">
                    <ul>
                        <a class="current" href="<?= get_post_type_archive_link('post') ?>"><li>Toutes</li></a>
                        <?php $categories = get_categories(); ?>
                        <?php foreach ($categories as $id => $category): ?>
                            <?php if($category->slug != 'non-classe'): ?>
								<a <?php if(get_queried_object()->name == $category->name) echo 'class="current"' ?> href="<?= get_category_link($category->term_id) ?>"><li><?= $category->name ?></li></a>
							<?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </aside>
        <?php else: ?>
            <h1><?= _e('Les Actualités') ?></h1>

            <div class="category__toggle">
                <svg class="anim-500 filter-open" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64 64" xml:space="preserve" width="64" height="64"><title>preferences</title><g class="nc-icon-wrapper" fill="#FFFFFF"><path fill="#FFFFFF" d="M62,9H31v2h31c0.552,0,1-0.448,1-1S62.552,9,62,9z"></path> <path fill="#FFFFFF" d="M26,1H16c-0.552,0-1,0.448-1,1v7H2c-0.552,0-1,0.448-1,1s0.448,1,1,1h13v7c0,0.552,0.448,1,1,1h10 c0.552,0,1-0.448,1-1V2C27,1.448,26.552,1,26,1z"></path> <path fill="#FFFFFF" d="M62,53H31v2h31c0.552,0,1-0.448,1-1S62.552,53,62,53z"></path> <path fill="#FFFFFF" d="M26,45H16c-0.552,0-1,0.448-1,1v7H2c-0.552,0-1,0.448-1,1s0.448,1,1,1h13v7c0,0.552,0.448,1,1,1h10 c0.552,0,1-0.448,1-1V46C27,45.448,26.552,45,26,45z"></path> <path data-color="color-2" d="M2,31h31v2H2c-0.552,0-1-0.448-1-1S1.448,31,2,31z"></path> <path data-color="color-2" d="M38,23h10c0.552,0,1,0.448,1,1v7h13c0.552,0,1,0.448,1,1s-0.448,1-1,1H49v7 c0,0.552-0.448,1-1,1H38c-0.552,0-1-0.448-1-1V24C37,23.448,37.448,23,38,23z"></path></g></svg>
            </div>

            <div class="category__sidebar">
                <ul>
                    <a class="current" href="<?= get_post_type_archive_link('post') ?>"><li>Toutes</li></a>
                    <?php $categories = get_categories(); ?>
                    <?php foreach ($categories as $id => $category): ?>
                        <?php if($category->slug != 'non-classe'): ?>
                            <a <?php if(get_queried_object()->name == $category->name) echo 'class="current"' ?> href="<?= get_category_link($category->term_id) ?>"><li><?= $category->name ?></li></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

        <div class="right-col">

            <?php if ( have_posts() ) : 
                while ( have_posts() ) :
                    the_post();
                    get_template_part( 'template-parts/content', get_post_type().'-archive' );
                endwhile;
                the_posts_navigation();
            else :
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>

        </div>    
    </div>


	</main><!-- #main -->

<?php
get_footer(null, 'd-none');
